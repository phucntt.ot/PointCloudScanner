using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class PointCloudScript : MonoBehaviour
{
    [SerializeField]
    ARCameraManager m_CameraManager;
    [SerializeField]
    AROcclusionManager m_OcclusionManager;
    [SerializeField]
    RawImage m_cameraView;
    [SerializeField]
    RawImage m_grayDepthView;
    [SerializeField]
    RawImage m_confidenceView;
    [SerializeField]
    Visualizer m_visualizer;

    [SerializeField]
    float near;
    [SerializeField]
    float far;

    [SerializeField]
    float a = 1;

    Texture2D m_CameraTexture;
    Texture2D m_DepthTexture_Float;
    Texture2D m_DepthTexture_BGRA;
    Texture2D m_DepthConfidenceTexture_R8;
    Texture2D m_DepthConfidenceTexture_RGBA;

    Vector3[] vertices = null;
    Color[] colors = null;
    Vector3[] m_CameraPosition;
    Quaternion[] m_CameraAngleEuler;

    float cx, cy, fx, fy;
    bool isScanning = true;
    bool cameraOn = false;

    void OnEnable()
    {
        if (m_CameraManager != null)
        {
            m_CameraManager.frameReceived += OnCameraFrameReceived;
        }
    }

    void OnDisable()
    {
        if (m_CameraManager != null)
        {
            m_CameraManager.frameReceived -= OnCameraFrameReceived;
        }
    }

    void OnCameraFrameReceived(ARCameraFrameEventArgs eventArgs)
    {
        UpdateCameraImage();
        UpdateEnvironmentDepthImage();
        cameraOn = true;
        //UpdateEnvironmentConfidenceImage();
        //if (isScanning)
        //{
        //    ReprojectPointCloud();
        //}
    }

    private void Start()
    {
        m_visualizer.transform.parent = m_CameraManager.transform;
        Application.lowMemory += OnLowMemory;
    }
    float elapsed = 0f;
    private void Update()
    {
        if (isScanning && cameraOn)
        {
            elapsed += Time.deltaTime;
            if (elapsed >= 1f)
            {
                elapsed = elapsed % 1f;
                ReprojectPointCloud();
            }
        }
    }

    private void OnLowMemory()
    {
        print("Low memory");
        m_visualizer.showMessage("Memory is low");
        m_visualizer.gameObject.SetActive(false);
        //StartCoroutine(m_visualizer.coroutineShowMessage("Memory is low...", 5f));
    }

    unsafe void UpdateCameraImage()
    {
        // Attempt to get the latest camera image. If this method succeeds,
        // it acquires a native resource that must be disposed (see below).
        if (!m_CameraManager.TryAcquireLatestCpuImage(out XRCpuImage image))
        {
            return;
        }

        using (image)
        {
            // Choose an RGBA format.
            // See XRCpuImage.FormatSupported for a complete list of supported formats.
            var format = TextureFormat.RGBA32;

            //Initialize m_CameraTexture if it's null or size of image is changed.
            if (m_CameraTexture == null || m_CameraTexture.width != image.width || m_CameraTexture.height != image.height)
            {
                m_CameraTexture = new Texture2D(image.width, image.height, format, false);

            }

            UpdateRawImage(m_CameraTexture, image, format);

            // Set the RawImage's texture so we can visualize it.
            m_cameraView.texture = m_CameraTexture;

        }
    }

    void UpdateEnvironmentDepthImage()
    {
        // Attempt to get the latest environment depth image. If this method succeeds,
        // it acquires a native resource that must be disposed (see below).
        if (!m_OcclusionManager.TryAcquireEnvironmentDepthCpuImage(out XRCpuImage image))
        {
            return;
        }

        using (image)
        {
            // If the texture hasn't yet been created, or if its dimensions have changed, (re)create the texture.
            // Note: Although texture dimensions do not normally change frame-to-frame, they can change in response to
            //    a change in the camera resolution (for camera images) or changes to the quality of the human depth
            //    and human stencil buffers.
            if (m_DepthTexture_Float == null || m_DepthTexture_Float.width != image.width || m_DepthTexture_Float.height != image.height)
            {
                m_DepthTexture_Float = new Texture2D(image.width, image.height, image.format.AsTextureFormat(), false);
            }
            if (m_DepthTexture_BGRA == null || m_DepthTexture_BGRA.width != image.width || m_DepthTexture_BGRA.height != image.height)
            {
                m_DepthTexture_BGRA = new Texture2D(image.width, image.height, TextureFormat.BGRA32, false);
            }

            //Acquire Depth Image (RFloat format). Depth pixels are stored with meter unit.
            UpdateRawImage(m_DepthTexture_Float, image, image.format.AsTextureFormat());

            //Convert RFloat into Grayscale Image between near and far clip area.
            ConvertFloatToGrayScale(m_DepthTexture_Float, m_DepthTexture_BGRA);
            //Visualize near~far depth.
            m_grayDepthView.texture = m_DepthTexture_BGRA;

        }
    }

    void UpdateEnvironmentConfidenceImage()
    {
        // Attempt to get the latest environment depth image. If this method succeeds,
        // it acquires a native resource that must be disposed (see below).
        if (!m_OcclusionManager.TryAcquireEnvironmentDepthConfidenceCpuImage(out XRCpuImage image))
        {
            return;
        }

        using (image)
        {
            if (m_DepthConfidenceTexture_R8 == null || m_DepthConfidenceTexture_R8.width != image.width || m_DepthConfidenceTexture_R8.height != image.height)
            {
                m_DepthConfidenceTexture_R8 = new Texture2D(image.width, image.height, image.format.AsTextureFormat(), false);
                print(image.format.AsTextureFormat());
            }
            if (m_DepthConfidenceTexture_RGBA == null || m_DepthConfidenceTexture_RGBA.width != image.width || m_DepthConfidenceTexture_RGBA.height != image.height)
            {
                m_DepthConfidenceTexture_RGBA = new Texture2D(image.width, image.height, TextureFormat.BGRA32, false);

            }
            UpdateRawImage(m_DepthConfidenceTexture_R8, image, image.format.AsTextureFormat());

            ConvertR8ToConfidenceMap(m_DepthConfidenceTexture_R8, m_DepthConfidenceTexture_RGBA);

            m_confidenceView.texture = m_DepthConfidenceTexture_RGBA;
        }
    }

    unsafe void UpdateRawImage(Texture2D texture, XRCpuImage cpuImage, TextureFormat format)
    {
        // For display, we need to mirror about the vertical access.
        var conversionParams = new XRCpuImage.ConversionParams(cpuImage, format, XRCpuImage.Transformation.MirrorY);

        // Get the Texture2D's underlying pixel buffer.
        var rawTextureData = texture.GetRawTextureData<byte>();

        // Make sure the destination buffer is large enough to hold the converted data (they should be the same size)
        Debug.Assert(rawTextureData.Length == cpuImage.GetConvertedDataSize(conversionParams.outputDimensions, conversionParams.outputFormat),
            "The Texture2D is not the same size as the converted data.");

        // Perform the conversion.
        cpuImage.Convert(conversionParams, rawTextureData);
        //cpuImage.Convert(conversionParams, new IntPtr(rawTextureData.GetUnsafePtr()), rawTextureData.Length);
        // "Apply" the new pixel data to the Texture2D.
        texture.Apply();
    }

    void ConvertFloatToGrayScale(Texture2D txFloat, Texture2D txGray)
    {

        //Conversion of grayscale from near to far value
        int length = txGray.width * txGray.height;
        Color[] depthPixels = txFloat.GetPixels();
        Color[] colorPixels = txGray.GetPixels();

        for (int index = 0; index < length; index++)
        {

            var value = (depthPixels[index].r - near) / (far - near);

            colorPixels[index].r = value;
            colorPixels[index].g = value;
            colorPixels[index].b = value;
            colorPixels[index].a = 1;
        }
        txGray.SetPixels(colorPixels);
        txGray.Apply();
    }

    void ConvertR8ToConfidenceMap(Texture2D txR8, Texture2D txRGBA)
    {
        Color32[] r8 = txR8.GetPixels32();
        Color32[] rgba = txRGBA.GetPixels32();
        for (int i = 0; i < r8.Length; i++)
        {
            switch (r8[i].r)
            {
                case 0:
                    rgba[i].r = 255;
                    rgba[i].g = 0;
                    rgba[i].b = 0;
                    rgba[i].a = 255;
                    break;
                case 1:
                    rgba[i].r = 0;
                    rgba[i].g = 255;
                    rgba[i].b = 0;
                    rgba[i].a = 255;
                    break;
                case 2:
                    rgba[i].r = 0;
                    rgba[i].g = 0;
                    rgba[i].b = 255;
                    rgba[i].a = 255;
                    break;
            }
        }
        txRGBA.SetPixels32(rgba);
        txRGBA.Apply();
    }

    public void ReprojectPointCloud()
    {
        //print("Depth:" + m_DepthTexture_Float.width + "," + m_DepthTexture_Float.height);
        //print("Color:" + m_CameraTexture.width + "," + m_CameraTexture.height);
        int width_depth = m_DepthTexture_Float.width;
        int height_depth = m_DepthTexture_Float.height;
        int width_camera = m_CameraTexture.width;

        if (vertices == null || colors == null)
        {
            vertices = new Vector3[width_depth * height_depth];
            colors = new Color[width_depth * height_depth];

            XRCameraIntrinsics intrinsic;
            m_CameraManager.TryGetIntrinsics(out intrinsic);
            print("intrinsics:" + intrinsic);

            float ratio = (float)width_depth / (float)width_camera;
            fx = intrinsic.focalLength.x * ratio;
            fy = intrinsic.focalLength.y * ratio;

            cx = intrinsic.principalPoint.x * ratio;
            cy = intrinsic.principalPoint.y * ratio;
        }

        Color[] depthPixels = m_DepthTexture_Float.GetPixels();

        int index_dst;
        float depth;
        for (int depth_y = 0; depth_y < height_depth; depth_y++)
        {
            index_dst = depth_y * width_depth;
            for (int depth_x = 0; depth_x < width_depth; depth_x++)
            {
                colors[index_dst] = m_CameraTexture.GetPixelBilinear((float)depth_x / (width_depth), (float)depth_y / (height_depth));

                depth = depthPixels[index_dst].r;
                if (depth > near && depth < far)
                {
                    vertices[index_dst].z = depth;
                    vertices[index_dst].x = -depth * (depth_x - cx) / fx;
                    vertices[index_dst].y = -depth * (depth_y - cy) / fy;
                }
                else
                {
                    vertices[index_dst].z = -999;
                    vertices[index_dst].x = 0;
                    vertices[index_dst].y = 0;
                }
                index_dst++;
            }
        }
        m_visualizer.createMeshInfo(vertices, colors);
        m_visualizer.showMessage("Scanning");
    }



    public void onProjectPointCloud()
    {
#if UNITY_EDITOR
        string path = Application.dataPath + "/Resources/Images/";
        //print(path);
        TextAsset image;
        byte[] normal_bytes = File.ReadAllBytes(path + "normal_1");
        byte[] depth_bytes = File.ReadAllBytes(path + "depth_1");

        m_CameraTexture = new Texture2D(1920, 1440, TextureFormat.RGBA32, false);
        m_CameraTexture.LoadRawTextureData(normal_bytes);
        m_CameraTexture.Apply();
        m_cameraView.texture = m_CameraTexture;

        m_DepthTexture_BGRA = new Texture2D(256, 192, TextureFormat.BGRA32, false);
        m_DepthTexture_BGRA.LoadRawTextureData(depth_bytes);
        m_DepthTexture_BGRA.Apply();
        m_grayDepthView.texture = m_DepthTexture_BGRA;


#else
        //print("Color:" + m_CameraTexture.width + "," + m_CameraTexture.height);
        //print("Depth BGRA:" + m_DepthTexture_BGRA.width + "," + m_DepthTexture_BGRA.height);
        //print("Depth Float:" + m_DepthTexture_Float.width + "," + m_DepthTexture_Float.height);
#endif
        print("take point cloud");
        //print("Color:" + m_CameraTexture.width + "," + m_CameraTexture.height);
        //print("Depth:" + m_DepthTexture_BGRA.width + "," + m_DepthTexture_BGRA.height);
        int width_depth = m_DepthTexture_Float.width;
        int height_depth = m_DepthTexture_Float.height;
        int width_camera = m_CameraTexture.width;
        if (vertices == null || colors == null)
        {
            vertices = new Vector3[width_depth * height_depth];
            colors = new Color[width_depth * height_depth];

#if !UNITY_EDITOR
            XRCameraIntrinsics intrinsic;
            m_CameraManager.TryGetIntrinsics(out intrinsic);
            print("intrinsics:" + intrinsic);

            float ratio = (float)width_depth / (float)width_camera;
            fx = intrinsic.focalLength.x * ratio;
            fy = intrinsic.focalLength.y * ratio;

            cx = intrinsic.principalPoint.x * ratio;
            cy = intrinsic.principalPoint.y * ratio;
#else
            print("Camera Editor");
            XRCameraIntrinsics intrinsic;
            m_CameraManager.TryGetIntrinsics(out intrinsic);
            //print("intrinsics:" + intrinsic);
            float ratio = (float)width_depth / (float)width_camera;
            fx = 1522.32f * ratio;
            fy = 1522.32f * ratio;

            cx = 953.39f * ratio;
            cy = 722.45f * ratio;

            //print(fx);

#endif
    }

        Color[] depthPixels = m_DepthTexture_Float.GetPixels();

        int index_dst;
        float depth;
        for (int depth_y = 0; depth_y < height_depth; depth_y++)
        {
            index_dst = depth_y * width_depth;
            for (int depth_x = 0; depth_x < width_depth; depth_x++)
            {
                colors[index_dst] = m_CameraTexture.GetPixelBilinear((float)depth_x / (width_depth), (float)depth_y / (height_depth));

                depth = depthPixels[index_dst].r;
                if (depth > near && depth < far)
                {
                    vertices[index_dst].z = depth;
                    vertices[index_dst].x = -depth * (depth_x - cx) / fx;
                    vertices[index_dst].y = -depth * (depth_y - cy) / fy;
                }
                else
                {
                    vertices[index_dst].z = -999;
                    vertices[index_dst].x = 0;
                    vertices[index_dst].y = 0;
                }
                index_dst++;
            }
        }

#if !UNITY_EDITOR
        //StartCoroutine(convertTextureAndUpload(m_CameraTexture, "normal"));
        //StartCoroutine(convertTextureAndUpload(m_DepthTexture_BGRA, "depth"));
        //StartCoroutine(convertTextureAndUpload(m_DepthTexture_Float, "depth_float"));
        //StartCoroutine(convertTextureAndUpload(m_DepthConfidenceTexture_RGBA, "confidence"));
#endif
        m_visualizer.createMeshInfo(vertices, colors);
    }

    public void SwitchScanMode(bool flg)
    {
        isScanning = flg;
        if (flg)
        {
            m_visualizer.transform.parent = m_CameraManager.transform;
            m_visualizer.transform.localPosition = Vector3.zero;
            m_visualizer.transform.localRotation = Quaternion.identity;
        }
        else
        {
            m_visualizer.transform.parent = null;
        }
    }



    IEnumerator convertTextureAndUpload(Texture2D texture, String fileName)
    {
        print("convert " + fileName);
        string screenshotFilename;
        string path;

        //string date = System.DateTime.Now.ToString("ddMMyyHHmmss");
        //screenshotFilename = fileName + "_" + date + ".png";
        screenshotFilename = fileName + "_1";

        string iosPath = Application.persistentDataPath + "/" + screenshotFilename;
        path = iosPath;

        byte[] bytes = texture.GetRawTextureData();
        File.WriteAllBytes(path, bytes);
        m_visualizer.uploadFirebase(bytes, screenshotFilename);
        yield return new WaitForEndOfFrame();
    }


}
