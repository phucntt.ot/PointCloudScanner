using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    Vector2 turn;
    
    private void Update()
    {
        if (Input.GetMouseButton(1))
        {
            turn.x += Input.GetAxis("Mouse X") * 2f;
            turn.y += Input.GetAxis("Mouse Y") * 2f;
            transform.localRotation = Quaternion.Euler(-turn.y, turn.x, 0);
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        float y = transform.position.y;
        Vector3 deltaMove = new Vector3(x, 0, z) * 6 * Time.deltaTime;
        transform.Translate(deltaMove);
        transform.position = new Vector3(transform.position.x, y, transform.position.z);

        if (Input.GetKey(KeyCode.Q))
        {
            transform.position -= new Vector3(0, 0.01f, 0);
        }
        else if(Input.GetKey(KeyCode.E))
        {
            transform.position += new Vector3(0, 0.01f, 0);

        }
    }
}
