using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Visualizer : MonoBehaviour
{
    [SerializeField] Material material;
    [SerializeField] Transform m_PointCloudGroup;
    [SerializeField] Text m_TextMessage;
    [SerializeField] Text m_TextDebug;

    Mesh mesh;
    int[] indices;

    int lastFrameIndex;
    float[] frameDeltaTimeArray;

    int m_CountPic;
    double m_PointCloudNo;
    float time;

    Vector3 m_Position;
    Quaternion m_Rotation;

    private void Awake()
    {
        m_PointCloudNo = 0;
        time = 0f;
        frameDeltaTimeArray = new float[50];
        m_TextMessage.text = "";
    }

    private void Update()
    {
        frameDeltaTimeArray[lastFrameIndex] = Time.deltaTime;
        lastFrameIndex = (lastFrameIndex + 1) % frameDeltaTimeArray.Length;

        var fps = Mathf.RoundToInt(CalculateFPS()).ToString();
        time += Time.deltaTime;
        //m_TextDebug.text = $"Picture: {m_CountPic} \nFPS : {fps} \nPointCloud: {m_PointCloudNo} \nTime: {time} ";
        m_TextDebug.text = $"Picture: {m_CountPic} \nPosition: {transform.position} \n Rotation: {transform.rotation} - {transform.eulerAngles}";
    }

    private float CalculateFPS()
    {
        float total = 0f;
        foreach (var deltaTime in frameDeltaTimeArray)
        {
            total += deltaTime;
        }
        return frameDeltaTimeArray.Length / total;
    }

    // Start is called before the first frame update

    public void UpdateMeshInfo(Vector3[] vertices, Color[] colors)
    {
        if (mesh == null)
        {

            mesh = new Mesh();
            mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

            //PointCloudの点の数はDepthのピクセル数から計算
            int num = vertices.Length;
            indices = new int[num];
            for (int i = 0; i < num; i++) { indices[i] = i; }

            //meshを初期化
            mesh.vertices = vertices;
            mesh.colors = colors;
            mesh.SetIndices(indices, MeshTopology.Points, 0);

            //meshを登場させる
            gameObject.GetComponent<MeshFilter>().mesh = mesh;
            m_Position = transform.position;
            m_Rotation = transform.rotation;

            print($"vertex: {vertices.Length} - color: {colors.Length}");
        }
        else
        {
            mesh.vertices = vertices;
            mesh.colors = colors;
            mesh.RecalculateBounds();

            //Vector3[] oldVertices = mesh.vertices;
            ////for (int i = 0; i < oldVertices.Length; i++)
            ////{
            ////    oldVertices[i] += m_Position;
            ////}
            //var newVertices = CombineVector3Arrays(oldVertices, vertices);

            //Color[] oldColor = mesh.colors;
            //Color[] mergeColor = oldColor.Concat(colors).ToArray();

            //print($"vertex: {newVertices.Length} - " +
            //    $"color : {mergeColor.Length}");

            //// create new colors array where the colors will be created.
            //Color[] newcolors = new Color[newVertices.Length];

            //for (int i = 0; i < vertices.Length; i++)
            //    newcolors[i] = Color.Lerp(Color.red, Color.green, vertices[i].y);

            //mesh.vertices = newVertices;
            //mesh.colors = newcolors;
            //mesh.RecalculateBounds();

            //m_Position = transform.position;
            //m_Rotation = transform.rotation;
            //print(newVertices[0] + " - " + oldVertices [0] + " - " + m_Position);

            //var mesh2 = new Mesh();
            //mesh2.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

            ////PointCloudの点の数はDepthのピクセル数から計算
            //int num = vertices.Length;
            //indices = new int[num];
            //for (int i = 0; i < num; i++) { indices[i] = i; }

            ////meshを初期化
            //mesh2.vertices = vertices;
            //mesh2.colors = colors;
            //mesh2.SetIndices(indices, MeshTopology.Points, 0);

            ////meshを登場させる
            //gameObject.GetComponent<MeshFilter>().mesh = mesh2;
        }
        m_CountPic++;
    }

    Vector3[] CombineVector3Arrays(Vector3[] array1, Vector3[] array2)
    {
        var array3 = new Vector3[array1.Length + array2.Length];
        System.Array.Copy(array1, array3, array1.Length);
        System.Array.Copy(array2, 0, array3, array1.Length, array2.Length);
        return array3;
    }

    public void createMeshInfo(Vector3[] vertices, Color[] colors)
    {
        var newMesh = new Mesh();
        newMesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;

        //PointCloudの点の数はDepthのピクセル数から計算
        int num = vertices.Length;
        indices = new int[num];
        for (int i = 0; i < num; i++) { indices[i] = i; }

        //meshを初期化
        newMesh.vertices = vertices;
        newMesh.colors = colors;
        newMesh.SetIndices(indices, MeshTopology.Points, 0);

        //meshを登場させる
        //gameObject.GetComponent<MeshFilter>().mesh = newMesh;

        var meshFilter = new GameObject("mesh", typeof(MeshRenderer)).AddComponent<MeshFilter>();
        meshFilter.mesh = newMesh;
        meshFilter.GetComponent<MeshRenderer>().material = material;
        meshFilter.transform.position = transform.position;
        meshFilter.transform.rotation = transform.rotation;
        meshFilter.transform.parent = m_PointCloudGroup;
        m_PointCloudNo += vertices.Length;
        //SaveAsset(meshFilter);
        //ExportFromPointCloud(vertices, colors);
        m_CountPic++;
    }

    public void onClearMesh()
    {
        for (int i = 0; i < m_PointCloudGroup.childCount; i++)
        {
            Destroy(m_PointCloudGroup.GetChild(0).gameObject);
        }
    }


    public void onButtonMergePointCloud()
    {

    }

    public void combineMesh()
    {
        MeshFilter[] meshFilters = m_PointCloudGroup.GetComponentsInChildren<MeshFilter>();
        CombineInstance[] combine = new CombineInstance[meshFilters.Length];

        int i = 0;
        while (i < meshFilters.Length)
        {
            combine[i].mesh = meshFilters[i].sharedMesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
            meshFilters[i].gameObject.SetActive(false);

            i++;
        }
        m_PointCloudGroup.GetComponent<MeshFilter>().mesh = new Mesh();
        m_PointCloudGroup.GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
        m_PointCloudGroup.gameObject.SetActive(true);
    }

#if UNITY_EDITOR
    void SaveAsset(MeshFilter mf)
    {
        if (mf)
        {
            var savePath = "Assets/Mesh/newMesh.asset";
            Debug.Log("Saved Mesh to:" + savePath);
            AssetDatabase.CreateAsset(mf.mesh, savePath);
        }
    }
#endif

    public void ExportFromPointCloud(Vector3[] vertices, Color[] colors)
    {
        //print("Export");
#if UNITY_EDITOR
        string path = Application.persistentDataPath + "/pointcloud.ply";
#elif UNITY_IOS
        string path = "Assets/Mesh/pointcloud.ply";
#endif
        if (!WriteHeader(vertices, colors, path))
            throw new ArgumentException("pc is invalid");

        WriteBody(vertices, colors, path);
    }

    // if write is fail, return false. else, true.
    private bool WriteHeader(Vector3[] vertices, Color[] colors, string path)
    {
        print("Write Header");
        int vertexNum = vertices.Length;
        if (vertexNum <= 0) return false;

        using (var sw = new StreamWriter(path, false, Encoding.ASCII))
        {
            sw.Write("ply\n" +
                     "format binary_little_endian 1.0\n" +
                     "element vertex " + vertexNum + "\n" +
                     "property float32 x\n" +
                     "property float32 y\n" +
                     "property float32 z\n");

            if (colors != null && colors.Length > 0)
            {
                sw.Write("property uchar red\n" +
                         "property uchar green\n" +
                         "property uchar blue\n");
            }

            sw.Write("end_header\n");
        }

        return true;
    }

    private void WriteBody(Vector3[] vertices, Color[] colors, string path)
    {
        print("Write Body");

        var fs = File.Open(path, FileMode.Append);

        using (var wr = new BinaryWriter(fs))
        {
            var verticesLen = vertices.Length;
            if (verticesLen <= 0) return;

            if (colors != null && colors.Length > 0)
            {
                wr.Write(ConvertToBytes(vertices, colors));
            }
            else
            {
                wr.Write(ConvertToBytes(vertices));
            }
        }

        Debug.Log("File save finished");

        //byte[] pointCloudFile = File.ReadAllBytes(path);
        //uploadFirebase(pointCloudFile, "pointcloudScan.ply");
    }

    public void onButtonUploadFirebase()
    {
        string path = Application.persistentDataPath + "/pointcloud.ply";
        byte[] pointCloudFile = File.ReadAllBytes(path);
        uploadFirebase(pointCloudFile, "pointcloudScan.ply");
    }

    private byte[] ConvertToBytes(Vector3[] vertices, Color[] colors)
    {
        var len = vertices.Length;
        var pointInfoByteSize = sizeof(float) * 3 + 3;
        var result = new byte[pointInfoByteSize * len];
        for (int i = 0; i < len; i++)
        {
            var idx = i * pointInfoByteSize;

            var x = BitConverter.GetBytes(vertices[i].x);
            result[idx + 0] = x[0];
            result[idx + 1] = x[1];
            result[idx + 2] = x[2];
            result[idx + 3] = x[3];

            var y = BitConverter.GetBytes(vertices[i].y);
            result[idx + 4] = y[0];
            result[idx + 5] = y[1];
            result[idx + 6] = y[2];
            result[idx + 7] = y[3];

            var z = BitConverter.GetBytes(vertices[i].z);
            result[idx + 8] = z[0];
            result[idx + 9] = z[1];
            result[idx + 10] = z[2];
            result[idx + 11] = z[3];

            result[idx + 12] = (byte)System.Math.Min(System.Math.Max(0, (int)(colors[i].r * 255)), 255);
            result[idx + 13] = (byte)System.Math.Min(System.Math.Max(0, (int)(colors[i].g * 255)), 255);
            result[idx + 14] = (byte)System.Math.Min(System.Math.Max(0, (int)(colors[i].b * 255)), 255);
        }

        return result;
    }

    private byte[] ConvertToBytes(Vector3[] src)
    {
        var len = src.Length;
        var pointInfoByteSize = sizeof(float) * 3;

        var result = new byte[pointInfoByteSize * len];
        for (var i = 0; i < len; i++)
        {
            var idx = i * 12;

            // x
            var x = BitConverter.GetBytes(src[i].x);
            result[idx + 0] = x[0];
            result[idx + 1] = x[1];
            result[idx + 2] = x[2];
            result[idx + 3] = x[3];

            // y
            var y = BitConverter.GetBytes(src[i].y);
            result[idx + 4] = y[0];
            result[idx + 5] = y[1];
            result[idx + 6] = y[2];
            result[idx + 7] = y[3];

            // z
            var z = BitConverter.GetBytes(src[i].z);
            result[idx + 8] = z[0];
            result[idx + 9] = z[1];
            result[idx + 10] = z[2];
            result[idx + 11] = z[3];
        }

        return result;
    }

    public void uploadFirebase(byte[] bytes, String screenshotFilename)
    {
        string url = "https://firebasestorage.clients6.google.com/v0/b/golfapp-e6d6a.appspot.com/o?name=" + screenshotFilename;
        try
        {
            var request = UnityWebRequest.Post(url, "");
            request.uploadHandler = (UploadHandler)new UploadHandlerRaw(bytes);
            request.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            //request.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
            StartCoroutine(onResponse(request));
            print("upload " + screenshotFilename);
        }
        catch (Exception e)
        {
            Debug.Log("ERROR : " + e.Message);
        }
    }

    private IEnumerator onResponse(UnityWebRequest req)
    {
        yield return req.SendWebRequest();
        if (req.isNetworkError)
            Debug.Log("Network error has occured: " + req.GetResponseHeader(""));
        else
        {
            Debug.Log("Success " + req.downloadHandler.text);
            StartCoroutine(coroutineShowMessage("File upload success", 5f));
        }
    }

    public IEnumerator coroutineShowMessage(string message, float delay)
    {
        m_TextMessage.text = message;
        m_TextMessage.enabled = true;
        yield return new WaitForSeconds(delay);
        m_TextMessage.enabled = false;
    }

    public void showMessage(string message)
    {
        m_TextMessage.enabled = true;
        m_TextMessage.text = message;
    }
}
